#pragma once

#include "Struct_Lexer.h"

TSymbol get_sym(ifstream &fin, int *attributes);
void generate_symbol_cat(int *attributes);
int ConstTabSearch(list<Const_Val> &TabConst, string buff);
void Add_Key_Words (list<Identy_Val> &TabKeyWords, list<string> keywords, int &WordCode);
int KeyTabSearch(list<Identy_Val> &TabKeyWords, string buff);
int IdentyTabSearch(list<Identy_Val> &TabIdenty, string buff);