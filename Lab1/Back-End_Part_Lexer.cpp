#include "Back-End_Part_Lexer.h"

TSymbol get_sym(ifstream &fin, int *attributes)
{
    TSymbol temp_sym;
    fin.get(temp_sym.value);
    temp_sym.attr = attributes[int(temp_sym.value)];
    return temp_sym;
}

void generate_symbol_cat(int *attributes)
{
    // unprohibited
    int i;
    for (i = 0; i <= 255; ++i)
        attributes[i] = 5;

    // space symbols
    attributes[32] = 0;
    for (i = 8; i <= 13; i++)
        attributes[i] = 0;
    // digits
    for (i = 48; i <= 57; i++)
        attributes[i] = 1;

    // letters
    for (i = 65; i <= 90; i++)
        attributes[i] = 2;
    
    // single char delimiters
    //attributes[41] = 4;  // )
    attributes[44] = 4; // ,
    attributes[46] = 4; // .
    attributes[58] = 4; // :
    attributes[59] = 4; // ;
    attributes[61] = 4; // =

    // multi-char delimiters
    // attributes[62] = 41; // >
    // attributes[60] = 42; // <

    // multi-char delimiters for comments
    attributes[40] = 3; // (
}

int ConstTabSearch(list<Const_Val> &TabConst, string buff)
{
    if (TabConst.empty())
    {
        return -1;
    }

    list<Const_Val>::iterator it;
    for (it = TabConst.begin(); it != TabConst.end(); ++it)
    {
        if (it->num == buff)
        {
            return it->code;
        }
    }
    return -1;
}

void Add_Key_Words (list<Identy_Val> &TabKeyWords, list<string> keywords, int &WordCode)
{
    Identy_Val buff;
    list<string>::iterator it;
    for (it = keywords.begin(); it != keywords.end(); ++it)
    {
        buff.name = *it;
        buff.code = WordCode;
        WordCode++;
        TabKeyWords.push_back(buff);
    }
}

int KeyTabSearch(list<Identy_Val> &TabKeyWords, string buff)
{
    if (TabKeyWords.empty())
    {
        return -1;
    }

    list<Identy_Val>::iterator it;
    for (it = TabKeyWords.begin(); it != TabKeyWords.end(); ++it)
    {
        if (it->name == buff)
        {
            return it->code;
        }
    }
    return -1;
}

int IdentyTabSearch(list<Identy_Val> &TabIdenty, string buff)
{
    if (TabIdenty.empty())
    {
        return -1;
    }

    list<Identy_Val>::iterator it;
    for (it = TabIdenty.begin(); it != TabIdenty.end(); ++it)
    {
        if (it->name == buff)
        {
            return it->code;
        }
    }
    return -1;
}