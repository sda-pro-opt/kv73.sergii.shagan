#pragma once

#include "Struct_Lexer.h"

void ShowTabLex(list<Lexem> TabLex, ofstream &out);
void ShowTabError(list<Lexem_Error> TabError, ofstream &out);
void WriteToFile(list<Lexem> TabLex, ofstream &out);
void ShowTabConst(list<Const_Val> TabConst, ofstream &out);
void ShowTabIdenty(list<Identy_Val> TabKeyWords, ofstream &out);