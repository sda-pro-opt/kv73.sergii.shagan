#include "Front-End_Part_Lexer.h"

void ShowTabConst(list<Const_Val> TabConst, ofstream &out)
{
    if (TabConst.empty())
    {
        cout << "TabConst is emty!" << endl;
        out << "TabConst is emty!" << endl;
        return;
    }

    list<Const_Val>::iterator it;
    for (it = TabConst.begin(); it != TabConst.end(); ++it)
    {
        cout << it->num << " --- " << it->code << endl;
        out << it->num << " --- " << it->code << endl;
    }
}

void ShowTabIdenty(list<Identy_Val> TabKeyWords, ofstream &out)
{
    if (TabKeyWords.empty())
    {
        cout << "TabIdenty is emty!" << endl;
        out << "TabIdenty is emty!" << endl;
        return;
    }

    list<Identy_Val>::iterator it;
    for (it = TabKeyWords.begin(); it != TabKeyWords.end(); ++it)
    {
        cout << it->name << " --- " << it->code << endl;
        out << it->name << " --- " << it->code << endl;
    }
}

void ShowTabLex(list<Lexem> TabLex, ofstream &out)
{
    if (TabLex.empty())
    {
        cout << "TabLex is emty!" << endl;
        out << "TabLex is emty!" << endl;
        return;
    }

    list<Lexem>::iterator it;
    for (it = TabLex.begin(); it != TabLex.end(); ++it)
    {
        cout << it->val << "  " << it->code << "  " << it->row << "  " << it->column << endl;
        out << it->val << "  " << it->code << "  " << it->row << "  " << it->column << endl;
    }
}

void ShowTabError(list<Lexem_Error> TabError, ofstream &out)
{
    if (TabError.empty())
    {
        cout << "TabError is emty!" << endl;
        out << "TabError is emty!" << endl;
        return;
    }

    list<Lexem_Error>::iterator it;
    for (it = TabError.begin(); it != TabError.end(); ++it)
    {
        switch (it->type)
        {
        case 1:
            cout << "Illegal symbol: " << it->val << " | code: " << it->code << " | row: " << it->row << " | column: " << it->column << endl;
            out << "Illegal symbol: " << it->val << " | code: " << it->code << " | row: " << it->row << " | column: " << it->column << endl;
            break;
        case 2:
            cout << "*) expected but end of file found | row: " << it->row << " | column: " << it->column << endl;
            out << "*) expected but end of file found | row: " << it->row << " | column: " << it->column << endl;
            break;
        case 3:
            cout << "Invalid constant value: " << it->val << " | row: " << it->row << " | column: " << it->column << endl;
            out << "Invalid constant value: " << it->val << " | row: " << it->row << " | column: " << it->column << endl;
        }
        
    }
}

void WriteToFile(list<Lexem> TabLex, ofstream &out)
{
    if (TabLex.empty())
    {
        out << "TabLex is emty!" << endl;
        return;
    }

    list<Lexem>::iterator it;
    for (it = TabLex.begin(); it != TabLex.end(); ++it)
    {
        out << it->code << " ";
    }
}