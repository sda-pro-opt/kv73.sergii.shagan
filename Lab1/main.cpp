#include "Front-End_Part_Lexer.h"
#include "Back-End_Part_Lexer.h"

list<string> keywords = {"PROGRAM", "LABEL", "BEGIN", "END", "GOTO", "IF", "ENDIF", "THEN", "ELSE"};

int main()
{

    bool SuppressOutput;
    bool flag_const;
    int row = 1;
    int column = 1;
    int ConstCode = 501;
    int WordCode = 401;
    int IdentyCode = 1001;
    int attributes[256];
    string buff;
    int LexCode;
    TSymbol symbol;
    Const_Val reserv;
    Identy_Val reserv1;
    Lexem Lex;
    Lexem_Error Error;
    list<Const_Val> TabConst;
    list<Identy_Val> TabKeyWords;
    list<Identy_Val> TabIndenty;
    list<Lexem> OutputLex;
    list<Lexem_Error> OutputError;


    Add_Key_Words(TabKeyWords, keywords, WordCode);
    string test;
    cout << "Please Enter folder (like 'Test/Test1'): " << endl;
    cin >> test;
    
    ifstream fin(test + "/input.sig");
    if (!fin.is_open())
    {
        cout << "Invalid Folder or input.sig isn`t search!" << endl;
        return 0;
    }
    
    ofstream out, gen;
    out.open(test + "/expected.txt");
    gen.open(test + "/generated.txt");
    generate_symbol_cat(attributes);
    if (fin.peek() == ifstream::traits_type::eof())
    {
        cout << "Emty file" << endl;
    }
    else
    {
        symbol = get_sym(fin, attributes);
        while (!fin.eof())
        {
            buff = "";
            LexCode = 0;
            SuppressOutput = false;
            switch (symbol.attr)
            {
            case 0:
                while (!fin.eof())
                {
                    if (symbol.value == '\n')
                    {
                        row++;
                        column = 0;
                    }
                    
                    symbol = get_sym(fin, attributes);
                    column++;

                    if (symbol.attr != 0)
                    {
                        SuppressOutput = true;
                        break;
                    }
                }
                break;
            case 1:
                Lex.row = row;
                Lex.column = column;
                flag_const = false;
                while ((!fin.eof()) && ((symbol.attr == 1) || (symbol.attr == 2)))
                {
                    buff = buff + symbol.value;
                    symbol = get_sym(fin, attributes);
                    column++;
                    if (symbol.attr == 2)
                    {
                        flag_const = true;
                    }
                }
                if (flag_const == true)
                {
                    Error.type = 3;
                    Error.val = buff;
                    Error.row = row;
                    Error.column = Lex.column;
                    OutputError.push_back(Error);
                    SuppressOutput = true;
                    break;
                }
                
                
                LexCode = ConstTabSearch(TabConst, buff);
                if (LexCode == -1)
                {
                    LexCode = ConstCode;
                    reserv.num = buff;
                    reserv.code = LexCode;
                    TabConst.push_back(reserv);
                    ConstCode++;
                }
                Lex.val = buff;
                Lex.code = LexCode;
                break;
            case 2:

                Lex.row = row;
                Lex.column = column;
                while ((!fin.eof()) && ((symbol.attr == 1) || (symbol.attr == 2)))
                {
                    buff = buff + symbol.value;
                    symbol = get_sym(fin, attributes);
                    column++;
                }
                LexCode = KeyTabSearch(TabKeyWords, buff);
                if (LexCode == -1)
                {
                    LexCode = IdentyTabSearch(TabIndenty, buff);
                    if (LexCode == -1)
                    {
                        LexCode = IdentyCode;
                        reserv1.name = buff;
                        reserv1.code = LexCode;
                        TabIndenty.push_back(reserv1);
                        IdentyCode++;
                    }
                    
                }
                Lex.val = buff;
                Lex.code = LexCode;
                break;
            case 3:
                if (fin.eof())
                {
                    Lex.val = symbol.value;
                    Lex.code = LexCode;
                    Lex.row = row;
                    Lex.column = column;
                }
                else
                {
                    Error.row = row;
                    Error.column = column;

                    symbol = get_sym(fin, attributes);
                    column++;
                    if (symbol.value == '*')
                    {
                        if (fin.eof())
                        {
                            Error.type = 2;
                            OutputError.push_back(Error);
                        }
                        else
                        {
                            symbol = get_sym(fin, attributes);
                            column++;
                            do
                            {
                                while ((!fin.eof()) && (symbol.value != '*'))
                                {
                                    symbol = get_sym(fin, attributes);
                                    column++;
                                }
                                if (fin.eof())
                                {
                                    Error.type = 2;
                                    OutputError.push_back(Error);
                                    break;
                                }
                                else
                                {
                                    symbol = get_sym(fin, attributes);
                                    column++;
                                }
                            } while (symbol.value != ')');
                            if (symbol.value == ')')
                            {
                                SuppressOutput = true;
                            }
                            if (!fin.eof())
                            {
                                symbol = get_sym(fin, attributes);
                                column++;
                            }
                            
                            
                        }
                    }
                    else
                    {
                        Error.val = "(";
                        Error.code = 40;
                        Error.row = row;
                        Error.column = column - 1;
                        Error.type = 1;
                        OutputError.push_back(Error);
                        SuppressOutput = true;
                    }
                }
                break;
            case 4:
                Lex.val = symbol.value;
                Lex.code = int(symbol.value);
                Lex.row = row;
                Lex.column = column;

                symbol = get_sym(fin, attributes);
                column++;
                break;
            case 5:
                Error.code = int(symbol.value);
                Error.val = symbol.value;
                Error.column = column;
                Error.row = row;
                Error.type = 1;
                OutputError.push_back(Error);

                SuppressOutput = true;
                symbol = get_sym(fin, attributes);
                column++;
                break;
            case 6:
                symbol = get_sym(fin, attributes);
                column++;
                break;
            }
            if ((SuppressOutput == false) && (!fin.eof()))
            {
                OutputLex.push_back(Lex);
            }
            
        }

    }

    WriteToFile(OutputLex, out);

    cout << "TabConst:" << endl;
    gen << "TabConst:" << endl;
    ShowTabConst(TabConst, gen);
    cout << endl;
    gen << endl;
    cout << "TabIdenty:" << endl;
    gen << "TabIdenty:" << endl;
    ShowTabIdenty(TabIndenty, gen);
    cout << endl;
    gen << endl;
    cout << "Output Lex:" << endl;
    gen << "Output Lex:" << endl;
    ShowTabLex(OutputLex, gen);
    cout << endl;
    gen << endl;
    cout << "Output Error:" << endl;
    gen << "Output Error:" << endl;
    ShowTabError(OutputError, gen);

    fin.close();
    out.close();
    gen.close();
    return 0;
}