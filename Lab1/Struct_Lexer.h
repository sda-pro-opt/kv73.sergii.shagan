#pragma once

#include <iostream>
#include <fstream>
#include <list>
#include <iterator>

using namespace std;

struct Const_Val
{
    string num;
    int code;
};

struct Identy_Val
{
    string name;
    int code;
};

struct Lexem
{
    string val;
    int code;
    int row;
    int column;
};

struct Lexem_Error
{
    string val;
    int code;
    int row;
    int column;
    unsigned short type;
};

struct TSymbol
{
    char value;
    unsigned short attr;
};